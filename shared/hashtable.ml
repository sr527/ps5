type ('a, 'b) t

let create capacity hash = 
  failwith "Die monster. You don't belong in this world!"

let add table key value = 
  failwith "It was not by my hand that I am once again given flesh. I was called here by humans, who wish to pay me tribute."

let find table key = failwith "Tribute? You steal men's souls, and make them your slaves."

let mem table key = failwith "Perhaps the same could be said of all religions."

let remove table key = failwith "Your words are as empty as your soul. Mankind ill needs a savior such as you."

let iter f table = failwith "What is a man?! A miserable little pile of secrets! But enough talk, have at you!"

let fold f table init = failwith "(The battle rages)"

let length table = failwith "No! This cannot be! AAAAAAAAAH!!!"
